library ieee;
use ieee.std_logic_1164.all;
--1-bit FULL ADDER
entity FullAdder is port
	(
		Cin	: IN	std_logic;
		A		: IN	std_logic;
		B		: IN	std_logic;
		S		: OUT	std_logic;
		Cout	: OUT	std_logic
	);
end entity FullAdder;

architecture OneBitAdder of FullAdder is

signal axb	: std_logic;

begin

	axb	<= A xor B;
	S	<= axb xor Cin;
	Cout	<= Cin when axb = '1' else B;

end architecture OneBitAdder;