library ieee;
use ieee.std_logic_1164.all;

entity C4M1P4 is port
	(
		-- Input ports
		SW		: in  std_logic_vector(9 downto 0);
		-- Output ports
		HEX0	: out std_logic_vector(7 downto 0);
		HEX1	: out std_logic_vector(7 downto 0);
		HEX2	: out std_logic_vector(7 downto 0);
		HEX3	: out std_logic_vector(7 downto 0);
		HEX4	: out std_logic_vector(7 downto 0);
		HEX5	: out std_logic_vector(7 downto 0);
		LEDR	: out std_logic_vector(9 downto 0)
	);
end entity C4M1P4;

architecture C4M1P4_imp of C4M1P4 is
	-- 4-bit Adder
	signal Error	: std_logic_vector(1 downto 0);
	signal Result	: std_logic_vector(9 downto 0);
	
	component C4M1P3 port
		(
			SW		: IN	std_logic_vector (9 downto 0);
			LEDR	: OUT	std_logic_vector (9 downto 0)
		);
	end component C4M1P3;
	-- Binary to BCD
	component C4M1P2 is port
		(
			SW		: IN	STD_LOGIC_VECTOR (9 downto 0);
			HEX0 	: OUT STD_LOGIC_VECTOR (7 downto 0);
			HEX1 	: OUT STD_LOGIC_VECTOR (7 downto 0)
		);
	end component C4M1P2;

	-- Declarations (optional)

begin
	
	-- Component Instantiation Statement (optional)
	--Error
	Error(0) <= (SW(3) and SW(1)) or (SW(3) and SW(2));
	Error(1) <= (SW(7) and SW(5)) or (SW(7) and SW(6));
	LEDR(9)	<= Error(1) or Error(0);
	-- SUM
	U1 : C4M1P3 port map (SW=>SW, LEDR=>Result);
	-- Bin 2 BCD
	U2 : C4M1P2 port map (SW=>Result							, HEX0=>HEX0, HEX1=>HEX1);
	U3 : C4M1P2 port map (SW=>"000000" & SW(3 downto 0), HEX0=>HEX2, HEX1=>HEX3);
	U4 : C4M1P2 port map (SW=>"000000" & SW(7 downto 4), HEX0=>HEX4, HEX1=>HEX5);
	-- Generate Statement (optional)

end architecture C4M1P4_imp;
